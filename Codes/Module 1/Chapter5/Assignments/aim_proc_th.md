# Virual Lab
![Logo](https://www.iitg.ac.in/cseweb/vlab/img/logo1.png)
## Aim
1. Introduction
## Aim :
The Learner will able to :
+ demonstrate working of PHP code.
+ create basic PHP scripts using proper syntax

<hr>

## Theory
#### Introduction
1. Introduction

PHP is widely used as an open source scripting language. PHP is an acronym for Hypertext Preprocessor. PHP scripts are executed on server and the HTML result is sent back to the client (Browser).PHP can generate dynamic page content. It can work with files on the server and can be used to collect form data. PHP can send and receive cookies. It can also work with database and can be easily embedded in HTML.PHP is widely used for Web Development.
<br>
PHP script generally contains HTML tags and PHP script. Every PHP script is stored with .php extension. PHP script can run on any platform (Windows, Linux,etc) and is compatible with most of the web servers (Apache,IIS,etc.). PHP has a support for wide range of databases with which one can work.
<br>
To start using PHP, we need a web server (host) with PHP and MySQL support along with PHP and MySQL installed on your PC or Laptop.
<br>
Any browser can be used to access the file with php code.One can access it with web server's URL and the file name at the end.(e.g. If the name of the file is *first.php* then to access this file locally one can use `http://localhost/first.php` as URL.)

2. Basic Syntax

PHP script can be placed anywhere in the document. PHP script starts with <**?php** and ends with **?**>.The script written between these two tags is parsed on the server and the plain HTML result is returned back. All php statements must end with **semicolon ( ; )**

<br>

< ?php <br>
code; <br>
?>

<br>

To run the script one needs a server enabled with PHP and a browser. PHP is loosely typed language.
*echo* or *print* is used to print the output.

<br>

E.g.following script will print "Welcome to PHP"

<br>

< ?php <br>
echo " Welcome to PHP " ;   <br>
?>

<br>

In PHP every variable starts with *$* sign . The data types supported by PHP are *String, Integer, Float Boolean, Array, Object etc.*

<br>

eg.     <br>
< ?php  <br>
\$number_1=10;   <br>
\$number_2=35.5; <br>
$str ="HELLO";  <br>
echo $number_1; <br>
echo $number_2; <br>
echo $str;      <br>
?> 

<br>

Here in PHP you don't have to specify data types as in other languages like C, C++,java etc. PHP converts the variable to the proper data type depending upon the value provided. i.e. in the example given above ***number_1*** will be integer and ***number_2*** will be float .Variable ***str*** will be of type String.

<hr>

## Procedure
#### Procedure

1. Demonstration
   1. Click the "Execute PHP" button and observe:
        1. step by step execution of PHP code.
        2. changes in HTML code and HTML page.
   2. Observe the location of the yellow band. It highlights the line being executed.
   3. Observe the output shown on the right hand side.
   4. Observe the html code generated during the execution at the server side, displayed below the PHP code.
2. Simulator
   <br>
   Write the PHP script for the tasks mentioned in the experiment and click "Execute" button to verify the output.

