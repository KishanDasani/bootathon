function area() {
    //getting radius from html file
    let radius = document.getElementById('radius');
    //getting the ans element from html file
    let ans = document.getElementById('ans');
    //getting the value of radius
    var radius1 = parseFloat(radius.value);
    console.log("Radius : " + radius1);
    //checking for radius to only be number
    //if not a number then showing message
    if (isNaN(radius1)) {
        ans.innerHTML = "The given Radius is not a number!!<br>Please enter a number as radius";
    }
    //is radius is a number, then calculating the area and sending back to html file
    else {
        //calculating area as PI*radius*radius
        var answer = Math.PI * Math.pow(radius1, 2);
        console.log("Area : " + answer);
        ans.innerHTML = "</b>The area of circle is :</b>" + answer.toString();
    }
}
//# sourceMappingURL=circle_area.js.map