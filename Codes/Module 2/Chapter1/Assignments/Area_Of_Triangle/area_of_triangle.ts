function triangle_area(){
    //getting the input from html file for vertice 1
    var i11 :  HTMLInputElement = <HTMLInputElement>document.getElementById('x1');
    var i12 :  HTMLInputElement = <HTMLInputElement>document.getElementById('y1');

    //getting the input from html file for vertice 2
    var i21 : HTMLInputElement = <HTMLInputElement>document.getElementById('x2');
    var i22 : HTMLInputElement = <HTMLInputElement>document.getElementById('y2');

    //getting the input from html file for vertice 3
    var i31 : HTMLInputElement = <HTMLInputElement>document.getElementById('x3');
    var i32 : HTMLInputElement = <HTMLInputElement>document.getElementById('y3');

    //getting value of vertice 1
    var x1 : number = +i11.value;
    var y1 : number = +i12.value;

    //getting value of vertice 2
    var x2 : number = +i21.value;
    var y2 : number = +i22.value;

    //getting value of vertice 3
    var x3 : number = +i31.value;
    var y3 : number = +i32.value;

    //checking if there is any non-number input from user
    //if there, then showing message
    if(isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3))
    {
        document.getElementById('ans').innerHTML = "Only numbers are allowed as vertices!<br>You may have entered values other than number once or more.";
    }
    //if all values are valid , then calculating the area from Heron's formula
    else
    {
        //getting the values of length of sides
        var side1 = Math.sqrt((Math.pow((x2-x1),2)) + (Math.pow((y2-y1),2)));
        console.log("side 1 : "+side1);
        var side2 = Math.sqrt((Math.pow((x3-x1),2)) + (Math.pow((y3-y1),2)));
        console.log("side 2 : "+side2);
        var side3 = Math.sqrt((Math.pow((x2-x3),2)) + (Math.pow((y2-y3),2)));
        console.log("side 3 : "+side3);

        //calculating semiperimeter
        var semiperimeter = (side1+side2+side3)/2;
        console.log("semiperimeter : "+semiperimeter);

        //calculating the area
        var area = Math.sqrt((semiperimeter) * (semiperimeter-side1) * (semiperimeter-side2) * (semiperimeter-side3));
        console.log("area : "+area);
        //sending back the result to html file
        document.getElementById('ans').innerHTML = "Area of triangle made by given vertices is : <br>" + area.toString();
    }
}
