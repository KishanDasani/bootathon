function add(){
    //getting inputs from html file
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");

    //getting values for input
    var a:number = +t1.value;
    var b:number = +t2.value;

    // Check for first input to be valid
    if(isNaN(a))
    {
        alert("First input is not a number");
    }   
    
    // Check for second input to be valid
    else if(isNaN(b))               
    {
        alert("Second input is not a number");
    }
    //everything is fine, then
    else
    {
        // Add to numbers
        var ans:number = a + b;

        //send result to html file
        p.innerHTML = "Addition is " + ans.toString();
    }   
}

// Substraction function
function sub(){                     
    //getting inputs from html file
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");

    //getting values for input
    var a:number = +t1.value;
    var b:number = +t2.value;

    // Check for first input to be valid
    if(isNaN(a))
    {
        alert("First input is not a number");
    }

    // Check for second input to be valid
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }

    //everything is fine, then
    else
    {
        // Subtract to numbers
        var ans:number = a - b;

        //send result to html file
        p.innerHTML += "<br>Substraction is " + ans.toString();
    }
}

// Multiplication function
function mul(){                   
    //getting inputs from html file  
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");

    //getting values for input
    var a:number = +t1.value;
    var b:number = +t2.value;

    // Check for first input to be valid
    if(isNaN(a))
    {
        alert("First input is not a number");
    }

    // Check for second input to be valid
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }
    //everything is fine, then
    else
    {
        // Multiply to numbers
        var ans:number = a * b;

        //send result to html file
        p.innerHTML += "<br>Multiplivcation is " + ans.toString();
    }
}
// Division function
function div(){     
    //getting inputs from html file                
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");

    //getting values for input
    var a:number = +t1.value;
    var b:number = +t2.value;

    // Check for first input to be valid
    if(isNaN(a))
    {
        alert("First input is not a number");
    }

    // Check for second input to be valid
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }

    // Condition for divide by zero
    else if(b == 0){
        alert("Not divide by zero");
    }

    //everything is fine, then
    else{
        // divide to numbers
        var ans:number = a / b;

        //send result to html file
        p.innerHTML += "<br>Division is " + ans.toString();
    }
}
function sin1(){                    // Sin function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    if(isNaN(+t1.value)){
        alert("Input is not a number");
    }
    else{
        var a:number = Math.PI/180*parseFloat(t1.value);        // Convert value into radian
        var ans:number = Math.sin(a);
        p.innerHTML += "<br>Sin of input is" + ans.toString();
    }
}
function cos1(){                // Cos function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    if(isNaN(+t1.value)){
        alert("First Input is not a number");
    }
    else{
        var a:number = Math.PI/180*parseFloat(t1.value);
        var ans:number = Math.cos(a);
        p.innerHTML += "<br>Cos of input is" + ans.toString();
    }
}
function tan1(){                // Tan function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    if(isNaN(+t1.value)){
        alert("Input is not a number");
    }
    else if(+t1.value==90)      // Handle exception case for tan90 that is infinite
    {
        alert("Tan90 is undefinded");
    }
    else{
        var a:number = Math.PI/180*parseFloat(t1.value);
        var ans:number = Math.tan(a);
        p.innerHTML += "<br>Tan of input is" + ans.toString();
    }
}
function sroot(){           // Square root function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    if(isNaN(a))
    {
        alert("Input is not a number");
    }
    else{
        var ans:number = Math.sqrt(a);
        p.innerHTML += "<br>Square root of input is" + ans.toString();
    }
}
function power(){           // power function
    let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var p:HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("p");
    var a:number = +t1.value;
    var b:number = +t2.value;
    if(isNaN(a))
    {
        alert("First input is not a number");
    }
    else if(isNaN(b))
    {
        alert("Second input is not a number");
    }
    else{
        var ans:number = Math.pow(a,b);
        p.innerHTML += "<br>Ans of Power is" + ans.toString();
    }
}
