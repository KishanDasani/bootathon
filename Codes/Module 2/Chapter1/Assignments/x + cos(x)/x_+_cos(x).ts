function xcos(){
    //getting the input element from html file
    var i1:HTMLInputElement = <HTMLInputElement>document.getElementById('i1');

    //getting mode input from html file
    var sel :HTMLInputElement =<HTMLInputElement> document.getElementById('select');

    //getting which option is selected
    var op = sel.value;
    console.log("selected value : " + op);
    //getting value of x from input
    var x :number = +i1.value;
    console.log("Value of x : " + x);
    //checking if x is entered as number
    //if not , then showing message
    if(isNaN(x))
    {
        document.getElementById('ans').innerHTML = "Given input is not a number!<br>Only number is allowed as input";
    }
    //otherwise calculating the result and sending back to html file
    else
    {
        //if degree option is choosen
        if(op == "deg")
        {
            var result  = x + Math.cos( x * Math.PI / 180);
            console.log("result in degree : " + result);
            document.getElementById('ans').innerHTML = "The value of X + Cos(x) for given value of X : " + x.toString() + " is : " + result.toString();            
        }
        //if radian option is choose
        else
        {
            var result  = x + Math.cos( x );
            console.log("result in radian : " + result);
            document.getElementById('ans').innerHTML = "The value of X + Cos(x) for given value of X : " + x.toString() + " is : " + result.toString();
        }
    }
}