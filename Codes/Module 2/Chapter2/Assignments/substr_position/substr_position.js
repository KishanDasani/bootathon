function substr() {
    //getting input elements from html file for substring
    var i1 = document.getElementById('i1');
    var i2 = document.getElementById('i2');
    //getting value of given string
    var data = i1.value;
    //getting value of given string for substring
    var str = i2.value;
    //finding the value of index for that string
    var pos = data.indexOf(str);
    //if position found to be valid, then 
    if (pos != -1) {
        //finding the substring
        var script = data.substr(pos, str.length);
        //and showing result
        document.getElementById('substr').innerHTML = "The given value <b>" + str + "</b> is extracted : <b>" + script + "</b>";
    }
    //if not found, then showing message
    else {
        document.getElementById('substr').innerHTML = "he given value <b>" + str + "</b> is not found";
    }
}
function indexof() {
    //getting input element from html file for string
    var i1 = document.getElementById('i1');
    //getting input from html file for char for indexof
    var i3 = document.getElementById('i3');
    //getting value of string
    var data = i1.value;
    console.log("Given String : " + data);
    //getting value of char to be found
    var char = i3.value;
    //determining value of index of given char input
    var pos = data.indexOf(char);
    //checking if char is available or not
    if (pos != -1) {
        //if available then sending result back to html file
        document.getElementById('indexof').innerHTML = "Given Value <b>" + char + "</b> is found at index : <b>" + pos + "</b>";
    }
    else {
        //if not available then sending error to html file
        document.getElementById('indexof').innerHTML = "Given Value <b>" + char + "</b> is not found";
    }
}
//# sourceMappingURL=substr_position.js.map