function upper(){
    //getting input element for string from html file
    var i1 : HTMLInputElement = <HTMLInputElement>document.getElementById('i1');

    //getting value of input
    var data = i1.value;
    console.log("Given string value : " + data);

    //converting the input to upper case and storing result in a variable
    var upper = data.toUpperCase();
    console.log("Upper case string : " + upper);

    //displaying result back to html file
    document.getElementById('upper').innerHTML = "Uppercased String : " + upper;
}

function lower(){
    //getting input element from html file
    var i1 : HTMLInputElement = <HTMLInputElement>document.getElementById('i1');
    
    //getting the value of input
    var data = i1.value;
    console.log("Given string value : " + data);

    //converting the input to lower case and storing the result to a variable
    var lower = data.toLowerCase();
    console.log("Lower case string : " + lower);

    //displaying the result back to html file
    document.getElementById('lower').innerHTML = "Lowercases String : " + lower;
}

function split(){
    //getting input element from html file
    var i1 : HTMLInputElement = <HTMLInputElement>document.getElementById('i1');

    //getting value of input
    var data = i1.value;
    console.log("Given string value : " + data);

    //splitting the data by space and stroing into a variable
    var split = data.split(" ");

    //as the result of split function will be array, iterating the array and showing result in html file
    for(var i=0; i<split.length;i++)
    {
        document.getElementById('split').innerHTML += "Splitted String (By Space) : " + split[i] + "<br>";
    }
    
}