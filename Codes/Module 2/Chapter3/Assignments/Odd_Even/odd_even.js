function oddeven() {
    //getting input from html file
    var i1 = document.getElementById('t1');
    //getting value of input
    var val = +i1.value;
    console.log("Given value : " + val);
    //checking that given input is number or not
    if (isNaN(val)) {
        //if not then sending error to html file
        document.getElementById('ans').innerHTML = "Given input is not a number!<br>Only number allowed as input";
    }
    //if valid input
    else {
        //then checking if it is dividable by 2
        if ((val % 2) == 0) {
            //if dividable then it is even and sending result back to
            document.getElementById('ans').innerHTML = "Given number <b>" + val + "</b> is <b>even</b>";
        }
        else {
            //if not dividable then it is odd and sending result back to
            document.getElementById('ans').innerHTML = "Given number <b>" + val + "</b> is <b>odd</b>";
        }
    }
}
//# sourceMappingURL=odd_even.js.map