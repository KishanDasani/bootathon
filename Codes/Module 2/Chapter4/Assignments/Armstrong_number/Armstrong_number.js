function check() {
    //iterating for loop from 100 to 999 for getting result
    for (var i = 100; i <= 999; i++) {
        //declaring a variable y same as i
        var y = i;
        //declaring a variable sum and initialize it to 0
        var sum = 0;
        //iterating the y variable through while loop until it is greater than 0
        while (y > 0) {
            //performing modulo operation as y%10 to store the last value in given value and storing in variable z
            var z = y % 10;
            console.log("z = y % 10 : " + z);
            //finding cube of z and adding it into the sum
            sum += Math.pow(z, 3);
            console.log("Sum : " + sum);
            //dividing the variable y with 10 and storing it's floor value back in y
            y = Math.floor(y / 10);
            console.log("y = math.flooe(y/10) : " + y);
        }
        //after iterations are over, checking for sum is equal as i means any value,
        if (sum == i) {
            //if both are equal, then armstrong number found 
            //thus displaying the result back to html file
            document.getElementById('ans').innerHTML += "<br><b>" + i.toString() + "</b>";
        }
    }
}
//# sourceMappingURL=Armstrong_number.js.map